/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelaInicialController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Label Celsius;
    @FXML
    private Button Botao;
    @FXML
    private Label Kelvin;
    @FXML
    private Label Fahrenheit;
    @FXML
    private TextField recebec;
    @FXML
    private TextField recebek;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
     private void converter()
     {
         int celsius,kelvin,fahren;
         if(!("".equals(recebec.getText())))
         {
            celsius=Integer.parseInt(recebec.getText());
            kelvin=celsius + 273;
            fahren=(18*celsius)/10 + 32;
            Celsius.setText(""+celsius);
            Kelvin.setText(""+kelvin);
            Fahrenheit.setText(""+fahren);
         }
         else
         {
            kelvin=Integer.parseInt(recebek.getText());
            celsius=kelvin - 273;
            fahren=(18*celsius)/10 + 32;
            Celsius.setText(""+celsius);
            Kelvin.setText(""+kelvin);
            Fahrenheit.setText(""+fahren); 
         }
     }
     @FXML
     private void apagarc()
     {
         recebec.setText("");
     }
     @FXML
      private void apagark()
     {
         recebek.setText("");
     }
    
}
